﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyController : MonoBehaviour {

    public int enemy_health = 10;
    public float attackRange;
    public bool enemyDead =false;
    public int deathCounter = 0;
    public float enemySpeed = 3f;

    private float bulletTimer = 2f;

    private BulletShooter bulletshooter;
    private GameObject player;
    private Transform playerPos;

    private Score score;

    //Start used for initialization

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerPos = player.transform;
        bulletshooter = gameObject.GetComponent<BulletShooter>();
        score = GameObject.Find("GameManager").GetComponent<Score>();
    }
    // Update is called once per frame
    void Update () {

        transform.position = Vector3.MoveTowards(transform.position, playerPos.position, enemySpeed * Time.deltaTime);

		if (enemy_health <=0)
        {
            Destroy(gameObject);
            enemyDead = true;
            score.deathval ++;
        }
        attackRange = Vector3.Distance(playerPos.position, transform.position);

        bulletTimer += Time.deltaTime;

        if (attackRange <= 6 && bulletTimer >= 2f)
        {
            Quaternion rotation = Quaternion.LookRotation(playerPos.position-transform.position);
            transform.rotation = rotation;

            bulletshooter.LaunchProjectile();
            bulletTimer = 0;
            
        }
	}
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            //enemy_health -= 10;
            enemy_health = enemy_health - 10;
        }
    }

}
