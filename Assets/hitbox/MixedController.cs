﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class MixedController : MonoBehaviour {



    public GameObject shooter;
    public Transform barrel;
    public GameObject bullet;
    public GameObject enemy;
    public float enemyHealth = 100;
    public Text heathText;

    public static MixedController instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        heathText.text = "" + enemyHealth;
    }

    // Update is called once per frame
    void Update () {
        heathText.text = "" + enemyHealth;
        Move();
        Shoot();

	}

    void Shoot()
    {

        if (Input.GetKeyDown("space"))
        {
            GameObject clonebullet = Instantiate(bullet, barrel.position, barrel.rotation) as GameObject;
            clonebullet.GetComponent<Rigidbody>().AddForce(transform.forward * 100);
            Physics.IgnoreCollision(clonebullet.GetComponent<Collider>(), enemy.GetComponent<Collider>());
            Destroy(clonebullet, 3f);
        }
    }

    void Move()
    {
        float Horizontal = Input.GetAxis("Horizontal");
        float Vertical = Input.GetAxis("Vertical");

        Vector3 move = new Vector3(Horizontal, Vertical, 0);

        shooter.transform.Translate(move * 2 * Time.deltaTime);

        Debug.DrawRay(shooter.transform.position, transform.TransformDirection(Vector3.forward) * 10);
    }

    public void HeathUpdate( string hitbox)
    {
        if (enemyHealth <= 0)
        {
            print(" Died by : " + hitbox);
            Destroy(enemy);
            enemyHealth = 0;
        }
    }
}
