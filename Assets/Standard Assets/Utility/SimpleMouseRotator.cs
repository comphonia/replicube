using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// � 2017 TheFlyingKeyboard and released under MIT License
// theflyingkeyboard.net
public class SimpleMouseRotator: MonoBehaviour
{
    public Vector3 mousePos;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = new Vector3(Input.mousePosition.x,
                                Input.mousePosition.y,
                                1f);
        Vector3 lookPos = Camera.main.ScreenToWorldPoint(mousePos);
        lookPos = lookPos - transform.position;
        float angle = Mathf.Atan2(lookPos.z, lookPos.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(-angle, Vector3.up);
    }

}