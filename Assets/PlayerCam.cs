﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCam : MonoBehaviour {

    public Transform target;
    public Vector3 offsetPos;
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(target.position.x + offsetPos.x,transform.position.y + offsetPos.y, target.position.z + offsetPos.z);
	}
}
