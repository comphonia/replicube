﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthColorCode : MonoBehaviour {

    public Color color_80_100;
    public Color color_60_79;
    public Color color_40_59;
    public Color color_20_39;
    public Color color_0_19;

    public GameObject player;
    private CubeController cubeController;

	// Use this for initialization
	void Start () {
        cubeController = player.GetComponent<CubeController>();
	}
	
	// Update is called once per frame
	void Update () {
        if(cubeController.player_health < 90 || cubeController.player_health == 100 )
        {
            player.GetComponent<Renderer>().material.color = color_80_100;
        }
        if (cubeController.player_health < 80)
        {
            player.GetComponent<Renderer>().material.color = color_60_79;
        }
        if (cubeController.player_health < 60)
        {
            player.GetComponent<Renderer>().material.color = color_40_59;
        }
        if (cubeController.player_health <40)
        {
            player.GetComponent<Renderer>().material.color = color_20_39;
        }
        if (cubeController.player_health <20)
        {
            player.GetComponent<Renderer>().material.color = color_0_19;
        }

    }
}
