﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum SpawnType { Timed, Wave, Number }
public class Spawner : MonoBehaviour
{

    public GameObject enemyPrefab;
    // Use this for initialization
    public float spawnTimer = 6f;

    public SpawnType spawnType = SpawnType.Number;
    [SerializeField]
    private CubeController cubecontroller;
    private GameObject player;
    public GameObject[] spawnedEnemies;

    void Start()
    {
        player = GameObject.Find("Player");
        cubecontroller = GameObject.Find("Player").GetComponent<CubeController>();
        spawnedEnemies = GameObject.FindGameObjectsWithTag("Enemy");
    }

    float i = 0;
    // Update is called once per frame
    void Update()
    {

        if (!cubecontroller.isDead)
        {
            spawnedEnemies = GameObject.FindGameObjectsWithTag("Enemy");

            i += Time.deltaTime;

            // waveType == Number
            if ( spawnType == SpawnType.Number && cubecontroller.isDead == false && spawnedEnemies.Length < 10)
            {
                GameObject Enemy = Instantiate(enemyPrefab, transform.position, transform.rotation);
                i = 0;
            }

            // waveType == Timed
           
                 if (spawnType == SpawnType.Timed && cubecontroller.isDead == false && i >= spawnTimer)
            {
                GameObject Enemy = Instantiate(enemyPrefab, transform.position, transform.rotation);
                i = 0;
            }
        }
    }

   
}
