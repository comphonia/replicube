﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathCam : MonoBehaviour {

    public GameObject mainCamera;
    public CubeController cubeController;


	
	// Update is called once per frame
	void Update () {

        if (cubeController.isDead == false)
        {
            Vector3 cam = mainCamera.transform.position;
            transform.position = cam;
        }
        else
        {}
    }
}
