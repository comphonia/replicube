﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxChecker : MonoBehaviour {
    public enum Hitmarker { Body,Head}
    public Hitmarker hitmarkerType;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(hitmarkerType == Hitmarker.Body && other.gameObject.tag == "bullet")
        {
            MixedController.instance.enemyHealth -= 10;
            MixedController.instance.HeathUpdate("body wounds");
            Destroy(other.gameObject);
        }

        if (hitmarkerType == Hitmarker.Head && other.gameObject.tag == "bullet")
        {
            MixedController.instance.enemyHealth -= 100;
            MixedController.instance.HeathUpdate("headshot");
            Destroy(other.gameObject);
        }
    }
}
