﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShooter : MonoBehaviour
{

    public GameObject playerBullet;
    public GameObject enemyBullet;
    public Transform barrel;
    public Transform enemyBarrel;

    // Update is called once per frame 
    void Update()
    {


        if (Input.GetKeyUp("space"))
        {
            GameObject bullet = Instantiate(playerBullet, barrel.position, barrel.rotation) as GameObject;
            bullet.GetComponent<Rigidbody>().AddForce(transform.forward * 500);
            Destroy(bullet, 1f);
        }

    }


    // Enemy shooter method

     public void LaunchProjectile()
    {
        GameObject bullet = Instantiate(enemyBullet, enemyBarrel.position, enemyBarrel.rotation) as GameObject;
        bullet.GetComponent<Rigidbody>().AddForce(transform.forward * 500);
        Destroy(bullet, 1f);

    }



    //END
}
